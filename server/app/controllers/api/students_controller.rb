class Api::StudentsController < ApplicationController
  def show
    @student = Student.find(params[:id])
    render json: @student, status: 200
  end

  def index
    render json: Student.all.order("student_id"), status: 200
  end

  def create
    student = Student.new(student_params)
    if student.save
      render json: student, status: 201
    else
      render json: { errors: student.errors.full_messages }, status: 422
    end
  end

  def update
    @student = Student.find(params[:id])
    if @student.update(student_params)
      render json: @student, status: 200
    else
      render json: { errors: @student.errors.full_messages }, status: 422
    end
  end

  def destroy
    @student = Student.find(params[:id])
    @student.destroy
    head 204
  end

  private
    def student_params
      params.require(:student).permit(:name, :student_id, :address, :birthday)
    end
end
